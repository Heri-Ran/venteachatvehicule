<?php

namespace App\Tests\Service\UseCases;

use App\Entity\Client;
use App\Service\UseCases\ICreate;
use \Mockery as m;

class ICreateTest extends \PHPUnit\Framework\TestCase
{
    public ICreate $icreateClient;

    public Client $client;

    public function nonEmptyClientProvider(): array
    {
        return [
            ["Mr", "Randria", "Jaona"],
            ["Mme", "Rabe", "Jeanne"],
            ["Ste", "Ramavo", ""]
        ];
    }

    protected  function setUp(): void
    {
        $this->icreateClient = new ICreate();

        $this->client = m::mock(Client::class);
    }

    public function testCreateEmptyClient(): void
    {
        $this->assertInstanceOf(Client::class, $this->icreateClient->query(Client::class));
    }

    /**
     * @dataProvider nonEmptyClientProvider
     */
    public function testCreateNonEmptyClient(string $civilite, string $nom, string $prenom): void
    {
        $this->client->shouldReceive("setLibelleCivilite")->with($civilite)->set("libelleCivilite", $civilite)->andReturn($this->client);
        $this->client->shouldReceive("getLibelleCivilite")->andReturn($civilite);
        $this->client->shouldReceive("setNom")->with($nom)->set("nom", $nom)->andReturn($this->client);
        $this->client->shouldReceive("getNom")->andReturn($nom);
        $this->client->shouldReceive("setPrenom")->with($prenom)->set("prenom", $prenom)->andReturn($this->client);
        $this->client->shouldReceive("getPrenom")->andReturn($prenom);

        $properties = [
            "libelleCivilite" => $civilite,
            "nom" => $nom,
            "prenom" => $prenom
        ];

        $this->assertEquals($this->client->getLibelleCivilite(), $this->icreateClient->query(Client::class, $properties)->getLibelleCivilite());
        $this->assertEquals($this->client->getNom(), $this->icreateClient->query(Client::class, $properties)->getNom());
        $this->assertEquals($this->client->getPrenom(), $this->icreateClient->query(Client::class, $properties)->getPrenom());
    }

    public function testCreateException(): void
    {
        $this->expectException(\Error::class);
        $this->expectExceptionMessage("Class \"Foo\" not found");
        dd($this->icreateClient->query("Foo"));
    }
}