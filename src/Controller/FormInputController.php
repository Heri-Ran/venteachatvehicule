<?php

namespace App\Controller;

use App\Service\Form\InputFileType;
use App\Service\UseCases\IUploadFileInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FormInputController extends AbstractController
{
    #[Route('/', name: 'app_form_input')]
    public function index(
        Request $request,
        ManagerRegistry $managerRegistry,
        IUploadFileInterface $iUploadFile
    ): Response
    {
        $form = $this->createForm(InputFileType::class);
        $successMessage = null;
        $errorMessage = null;
        $detailsError = null;
        $form->handleRequest($request);

        if($form->isSubmitted()){
            try{
                $informations = $iUploadFile->query($form->getData()["Fichier"]);
                $objectManager = $managerRegistry->getManager();

                foreach ($informations as $list) {
                    foreach ($list as $object){
                        $objectManager->persist($object);
                        $objectManager->flush();
                    }
                }

                $successMessage = "Fichier enregistré avec succès";
            } catch (\Exception $e) {
                $errorMessage = "Une erreur est survenu lors de l'import du fichier";
                $detailsError = $e->getTraceAsString();
            }
        }

        return $this->render('form_input/index.html.twig', [
            "form" => $form->createView(),
            "error_message" => $errorMessage,
            "success_message" => $successMessage,
            "details_error" => $detailsError
        ]);
    }
}