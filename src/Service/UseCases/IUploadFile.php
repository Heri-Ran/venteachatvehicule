<?php

namespace App\Service\UseCases;

use App\Entity\Achat;
use App\Entity\Client;
use App\Entity\Evenement;
use App\Entity\Vehicule;
use App\Entity\Vente;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class IUploadFile implements IUploadFileInterface
{
    private ICreateInterface $iCreate;

    public function __construct(ICreateInterface $iCreate)
    {
        $this->iCreate = $iCreate;
    }

    public function query(UploadedFile $file): array
    {
        $result = [];

        $fileFolder = __DIR__."\\..\\..\\..\\public\\uploads\\";
        $fileName = md5(uniqid()) . $file->getClientOriginalName();

        $file->move($fileFolder, $fileName);

        $spreadsheet = IOFactory::load($fileFolder . $fileName);
        $spreadsheet->getActiveSheet()->removeRow(1);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        //Ne pas prendre en compte le denier élément (à cause des colonnes null)
        unset($sheetData[count($sheetData)]);

        foreach ($sheetData as $row) {
            $clientProperties = [
                "numeroDeFiche" => $row["D"],
                "libelleCivilite" => $row["E"],
                "nom" => $row["G"],
                "prenom" => $row["H"],
                "numeroEtNomDeLaVoie" => $row["I"],
                "complementAdresse1" => $row["J"],
                "codePostal" => $row["K"],
                "ville" => $row["L"],
                "telephoneDomicile" => $row["M"],
                "telephonePortable" => $row["N"],
                "telephoneJob" => $row["O"],
                "email" => $row["P"]
            ];

            $vehiculeProperties = [
                "libelleMarque" => $row["T"],
                "libelleModele" => $row["U"],
                "version" => $row["V"],
                "vin" => $row["W"],
                "immatriculation" => $row["X"],
                "kilometrage" => $row["Z"],
                "libelleEnergie" => $row["AA"]
            ];

            $evenementProperties = [
                "compteEvenement" => $row["B"],
                "compteDernierEvenement" => $row["C"],
                "origineEvenement" => $row["AI"],
                "dateEvenement" => is_null($row["AH"]) ? null : date_create(date("d-m-Y", strtotime($row["AH"]))), // \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row["AH"]), //\DateTime::createFromFormat("dd-MM-yyyy", date("d-m-Y", strtotime($row["AH"]))),
                "dateDernierEvenement" => is_null($row["S"]) ? null : date_create(date("d-m-Y", strtotime($row["S"])))//\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row["S"])//\DateTime::createFromFormat("dd-MM-yyyy", date("d-m-Y", strtotime($row["S"])))
            ];

            $achatProperties = [
                "compteAffaire" => $row["A"],
                "dateAchat" => is_null($row["R"]) ? null : date_create(date("d-m-Y", strtotime($row["R"]))),
                "commentaireFacture" => $row["AD"],
                "intermediaireDeVente" => $row["AG"]
            ];

            $venteProperties = [
                "dateDeMiseEnCirculation" => is_null($row["Q"]) ? null : date_create(date("d-m-Y", strtotime($row["Q"]))),
                "vendeur" => is_null($row["AB"]) ? $row["AC"] : $row["AB"],
                "type" => $row["AE"],
                "numeroDeDossierVnVo" => $row["AF"]
            ];

            $result["Vente"][] = $this->iCreate->query(Vente::class, $venteProperties)
                                        ->addEvenement($this->iCreate->query(Evenement::class, $evenementProperties))
                                        ->addVehicule($this->iCreate->query(Vehicule::class, $vehiculeProperties))
                                        ->setAchat($this->iCreate->query(Achat::class, $achatProperties)->setClient($this->iCreate->query(Client::class, $clientProperties)))
            ;
        }

        return $result;
    }
}