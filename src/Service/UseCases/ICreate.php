<?php

namespace App\Service\UseCases;

class ICreate implements ICreateInterface
{

    public function query(string $class, ?array $properties = null): object
    {
        $instance = new $class();

        if(!is_null($properties))
            foreach ($properties as $index => $value) {
                $setter = new \ReflectionMethod($class, "set".ucfirst($index));
                $setter->invoke($instance, $value);
            }

        return $instance;
    }
}