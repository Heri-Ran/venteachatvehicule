<?php

namespace App\Entity;

use App\Repository\VenteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VenteRepository::class)]
class Vente
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateDeMiseEnCirculation = null;

    #[ORM\Column(length: 30, nullable: true)]
    private ?string $vendeur = null;

    #[ORM\Column(length: 2, nullable: true)]
    private ?string $type = null;

    #[ORM\Column(length: 10, nullable: true)]
    private ?string $numeroDeDossierVnVo = null;

    #[ORM\ManyToMany(targetEntity: Evenement::class, inversedBy: 'ventes', cascade: ["persist"])]
    private Collection $evenement;

    #[ORM\ManyToMany(inversedBy: 'vente', targetEntity: Vehicule::class, cascade: ["persist"])]
    private Collection $vehicule;

    #[ORM\OneToOne(mappedBy: 'vente', cascade: ['persist', 'remove'])]
    private ?Achat $achat = null;

    public function __construct()
    {
        $this->evenement = new ArrayCollection();
        $this->vehicule = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDeMiseEnCirculation(): ?\DateTimeInterface
    {
        return $this->dateDeMiseEnCirculation;
    }

    public function setDateDeMiseEnCirculation(?\DateTimeInterface $dateDeMiseEnCirculation): self
    {
        $this->dateDeMiseEnCirculation = $dateDeMiseEnCirculation;

        return $this;
    }

    public function getVendeur(): ?string
    {
        return $this->vendeur;
    }

    public function setVendeur(?string $vendeur): self
    {
        $this->vendeur = $vendeur;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getNumeroDeDossierVnVo(): ?string
    {
        return $this->numeroDeDossierVnVo;
    }

    public function setNumeroDeDossierVnVo(?string $numeroDeDossierVnVo): self
    {
        $this->numeroDeDossierVnVo = $numeroDeDossierVnVo;

        return $this;
    }

    /**
     * @return Collection<int, Evenement>
     */
    public function getEvenement(): Collection
    {
        return $this->evenement;
    }

    public function addEvenement(Evenement $evenement): self
    {
        if (!$this->evenement->contains($evenement)) {
            $this->evenement->add($evenement);
        }

        return $this;
    }

    public function removeEvenement(Evenement $evenement): self
    {
        $this->evenement->removeElement($evenement);

        return $this;
    }

    /**
     * @return Collection<int, Vehicule>
     */
    public function getVehicule(): Collection
    {
        return $this->vehicule;
    }

    public function addVehicule(Vehicule $vehicule): self
    {
        if (!$this->vehicule->contains($vehicule)) {
            $this->vehicule->add($vehicule);
            $vehicule->addVente($this);
        }

        return $this;
    }

    public function removeVehicule(Vehicule $vehicule): self
    {
        if ($this->vehicule->removeElement($vehicule)) {
            // set the owning side to null (unless already changed)
            if ($vehicule->getVente() === $this) {
                $vehicule->setVente(null);
            }
        }

        return $this;
    }

    public function getAchat(): ?Achat
    {
        return $this->achat;
    }

    public function setAchat(?Achat $achat): self
    {
        // unset the owning side of the relation if necessary
        if ($achat === null && $this->achat !== null) {
            $this->achat->setVente(null);
        }

        // set the owning side of the relation if necessary
        if ($achat !== null && $achat->getVente() !== $this) {
            $achat->setVente($this);
        }

        $this->achat = $achat;

        return $this;
    }
}
