<?php

namespace App\Entity;

use App\Repository\AchatRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AchatRepository::class)]
class Achat
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 10, nullable: true)]
    private ?string $compteAffaire = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateAchat = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $commentaireFacture = null;

    #[ORM\Column(length: 30, nullable: true)]
    private ?string $intermediaireDeVente = null;

    #[ORM\ManyToOne(inversedBy: 'achats', cascade: ["persist"])]
    private ?Client $client = null;

    #[ORM\OneToOne(inversedBy: 'achat', cascade: ['persist', 'remove'])]
    private ?Vente $vente = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompteAffaire(): ?string
    {
        return $this->compteAffaire;
    }

    public function setCompteAffaire(?string $compteAffaire): self
    {
        $this->compteAffaire = $compteAffaire;

        return $this;
    }

    public function getDateAchat(): ?\DateTimeInterface
    {
        return $this->dateAchat;
    }

    public function setDateAchat(?\DateTimeInterface $dateAchat): self
    {
        $this->dateAchat = $dateAchat;

        return $this;
    }

    public function getCommentaireFacture(): ?string
    {
        return $this->commentaireFacture;
    }

    public function setCommentaireFacture(?string $commentaireFacture): self
    {
        $this->commentaireFacture = $commentaireFacture;

        return $this;
    }

    public function getIntermediaireDeVente(): ?string
    {
        return $this->intermediaireDeVente;
    }

    public function setIntermediaireDeVente(?string $intermediaireDeVente): self
    {
        $this->intermediaireDeVente = $intermediaireDeVente;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getVente(): ?Vente
    {
        return $this->vente;
    }

    public function setVente(?Vente $vente): self
    {
        $this->vente = $vente;

        return $this;
    }
}
